﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TestApp.Annotations;

namespace TestApp
{
    class TestViewModel : INotifyPropertyChanged
    {
        private string _targetUri;
        private string _pageContent;
        private string _markdownContent;
        private string _pageTitle;
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public TestViewModel(string target = null)
        {
            TargetUri = target ?? "http://www.nuget.org/";
            PageTitle = "WpfBrowser Test App";
        }

        public string TargetUri
        {
            get { return _targetUri; }
            set
            {
                //if (Equals(value, _targetUri)) return;
                _targetUri = value;
                OnPropertyChanged();
            }
        }

        public string MarkdownContent
        {
            get { return _markdownContent; }
            set
            {
                //if (value == _markdownContent) return;
                _markdownContent = value;
                OnPropertyChanged();
            }
        }

        public string PageTitle
        {
            get { return _pageTitle; }
            set
            {
                if (value == _pageTitle) return;
                _pageTitle = value;
                OnPropertyChanged();
            }
        }

        public static string SamplePageContent => "content://<html><head><head/><body><h2>Test Content</h2></body></html>";
        public ICommand TestContentCommand => new RelayCommand(param => TargetUri = SamplePageContent);
        public ICommand TestUriCommand => new RelayCommand(param => this.TargetUri = "http://www.novell.com");

        public static string SampleMarkdownContent =>
@"# Heading 1
## Heading 2

Some text

[Maybe a](link)
[And a real one](http://www.google.com/)
[And a md link](./LayoutEngine.md)";

        public ICommand TestMarkdownContentCommand => new RelayCommand(param => MarkdownContent = SampleMarkdownContent);
    }
}
