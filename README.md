# WpfBrowser

A versatile, MVVM-compatible replacement for WPFs WebBrowser control, with full support for data binding and responsive UI behaviour. Also includes an MVVM-compatible MarkdownBrowser control, powered by Strapdown.js!

## Getting started

To install WpfBrowser, run the following command in the [Package Manager Console](http://docs.nuget.org/docs/start-here/using-the-package-manager-console)

```
Install-Package WpfBrowser
```

Now, from within your WPF app, add the following namespace:

```
xmlns:wpfBrowser="clr-namespace:WpfBrowser;assembly=WpfBrowser"
```
(you can change the prefix if you like)

And finally, add a browser control:
```xml
<wpfBrowser:WebBrowser Grid.Row="1"
	Target="{Binding Path=TargetUri, Mode=TwoWay}"
	ShowAddressBar="True"
	ShowNavigationButtons="True" />
```

**Bam**, MVVM-compatible WebBrowser control.

## Credits
Thanks to [Josh Smith](https://joshsmithonwpf.wordpress.com/about/) for the RelayCommand implementation, and [Icons8](https://icons8.com/) for the great icons.