﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Navigation;

namespace WpfBrowser
{
    /// <summary>
    ///  Interaction logic for WebBrowser.xaml
    /// </summary>
    public partial class WebBrowser : UserControl
    {
        private static bool EventHandled { get; set; }

        public WebBrowser()
        {
            InitializeComponent();
            Browser.Navigating += BrowserOnNavigating;
            Browser.Navigated += BrowserOnNavigated;
            UpdateGridVisibility();
        }

        private void BrowserOnNavigated(object o, NavigationEventArgs e)
        {
            //EventHandled = true;
            AddressBox.Text = e.Uri.ToString();
        }

        private static Visibility ToVisibility(bool shouldShow, Visibility ifTrue = Visibility.Visible,
            Visibility ifFalse = Visibility.Collapsed)
        {
            return (shouldShow) ? ifTrue : ifFalse;
        }

        private void BrowserOnNavigating(object sender, NavigatingCancelEventArgs navigatingCancelEventArgs)
        {
            //Debug.WriteLine(("--Navigating!"));
            //Debug.WriteLine($"Navigator: {navigatingCancelEventArgs.Navigator}");
            //Debug.WriteLine($"Mode: {navigatingCancelEventArgs.NavigationMode}");
            //Debug.WriteLine($"Initiated: {navigatingCancelEventArgs.IsNavigationInitiator}");
            //Debug.WriteLine("====");
        }

        public static readonly DependencyProperty ShowAddressBarProperty = DependencyProperty.Register(
            "ShowAddressBar", typeof(bool), typeof(WebBrowser), new FrameworkPropertyMetadata(default(bool))
            {
                AffectsRender = true,
                PropertyChangedCallback = (o, args) => ((WebBrowser)o).ShowAddressBar = (bool)args.NewValue
            });

        public bool ShowAddressBar
        {
            get { return (bool)GetValue(ShowAddressBarProperty); }
            set
            {
                SetValue(ShowAddressBarProperty, value);
                UpdateGridVisibility();
            }
        }

        public static readonly DependencyProperty ShowNavigationButtonsProperty = DependencyProperty.Register(
            "ShowNavigationButtons", typeof(bool), typeof(WebBrowser), new FrameworkPropertyMetadata(default(bool))
            {
                AffectsRender = true,
                PropertyChangedCallback = delegate (DependencyObject o, DependencyPropertyChangedEventArgs args)
                {
                    ((WebBrowser)o).ShowNavigationButtons = (bool)args.NewValue;
                }
            });

        public bool ShowNavigationButtons
        {
            get { return (bool)GetValue(ShowNavigationButtonsProperty); }
            set
            {
                SetValue(ShowNavigationButtonsProperty, value);
                UpdateGridVisibility();
            }
        }

        private void UpdateGridVisibility()
        {
            AddressBox.Visibility = ToVisibility(ShowAddressBar);
            //TODO nav button visibility
            HeaderGrid.Visibility = ToVisibility(ShowAddressBar || ShowNavigationButtons);
        }

        public static readonly DependencyProperty TargetProperty = DependencyProperty.Register(
            "Target", 
            typeof (string), 
            typeof (WebBrowser), 
            new FrameworkPropertyMetadata(default(string))
            {
                BindsTwoWayByDefault = true,
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                PropertyChangedCallback = UriPropertyChanged
            });

        public string Target
        {
            get { return (string) GetValue(TargetProperty); }
            set { SetValue(TargetProperty, value); }
        }
        private static void UriPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            var browser = (WebBrowser)o;
            if (EventHandled)
            {
                EventHandled = false;
                return;
            }
            var str = args.NewValue as string;
            if (str == null) return;
            var result = false;
            if (str.StartsWith("content://")) LoadStringContent(browser, (string) args.NewValue, out result);
            if (str.StartsWith("file://")) LoadFileContent(browser, (string) args.NewValue, out result);
            if (str.StartsWith("http"))
            {
                browser.Browser.Navigate(new Uri(str));
                result = true;
            }
            // ReSharper disable once NotResolvedInText
            if (!result) throw new ArgumentException(Properties.Resources.ExMsg_UnknownUriScheme, @"Target");
        }

        private static void LoadFileContent(WebBrowser browser, string path, out bool result)
        {
            try
            {
                var content = File.ReadAllText(path);
                browser.Browser.NavigateToString(content);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
        }

        private static void LoadStringContent(WebBrowser browser, string target, out bool result)
        {
            browser.Browser.NavigateToString(target.Substring(10));
            result = true;
        }

        private void RefreshButton_OnClick(object sender, RoutedEventArgs e)
        {
            Browser.Refresh(true);
        }
    }
}