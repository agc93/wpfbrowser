﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// ReSharper disable InconsistentNaming

namespace WpfBrowser.Content
{
    partial class MarkdownBrowserPage
    {
        private readonly string DocFile;
        private readonly MarkdownTheme Theme;
        private readonly string Title;
        public MarkdownBrowserPage(string title, string content, MarkdownTheme theme)
        {
            DocFile = content;
            Theme = theme;
            Title = title;
        }
    }
}
