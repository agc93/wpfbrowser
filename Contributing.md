# Contributing

I welcome any and all contributions, no matter how large or small, as long as you [play nice with others](https://thoughtbot.com/open-source-code-of-conduct).

## Getting started

First, fork the repo, then clone

```
git clone https://bitbucket.org/<your-username-here>/wpfbrowser.git
```
and open up the WpfControls solution. 

There are basically no external dependencies so you should be good to build and run straight away.

## Contributions

This repository uses the [git-flow workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) to manage new features and old fixes. To start on a new feature just run `git flow feature start`, give it a name and get developing.

If you think your feature is ready-to-go, run `git flow feature finish` to merge back to `develop` and open a pull request.

If you want to get some help or feedback, open your pull request, but don't finish the feature first. That way, we can keep track of changes a little better.

## Reporting issues

If you think you've found a bug, make sure you can reproduce it, and [open an issue](https://bitbucket.org/agc93/wpfbrowser/issues/new) with as much detail as you can. I (or another community member) will assess it as soon as we can.
