﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Navigation;
using WpfBrowser.Content;

namespace WpfBrowser
{
    /// <summary>
    ///  Interaction logic for MarkdownBrowser.xaml
    /// </summary>
    public partial class MarkdownBrowser : UserControl
    {
        private static MarkdownTheme StaticTheme;
        private static string StaticPageTitle;
        public MarkdownBrowser() : this(false) {}

        public MarkdownBrowser(bool showNavigationButtons)
        {
            InitializeComponent();
            this.BrowserControl.ShowAddressBar = false;
            BrowserControl.ShowNavigationButtons = showNavigationButtons;
            BrowserControl.Browser.Navigating += BrowserOnNavigating;
        }

        private void BrowserOnNavigating(object sender, NavigatingCancelEventArgs args)
        {
            try
            {
                //// ReSharper disable once InvertIf
                //if ((args.Uri.Scheme.Contains("about")) || (RootPath != null && args.Uri.AbsolutePath.StartsWith(RootPath)))
                //{
                //    args.Cancel = true;
                //    MarkdownContent = File.ReadAllText(args.Uri.AbsolutePath);
                //}
                if (args.Uri != null && (args.Uri.Scheme.Contains("about") || args.Uri.AbsolutePath.EndsWith(".md")))
                {
                    MarkdownContent = File.ReadAllText(args.Uri.AbsolutePath);
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public static readonly DependencyProperty PageTitleProperty = DependencyProperty.Register(
            "PageTitle", typeof (string), typeof (MarkdownBrowser), new FrameworkPropertyMetadata(default(string))
            {
                PropertyChangedCallback = (o, args) =>
                {
                    ((MarkdownBrowser) o).PageTitle = (string) args.NewValue;
                    StaticPageTitle = (string) args.NewValue;
                },
                BindsTwoWayByDefault = true
            });

        public string PageTitle
        {
            get { return (string) GetValue(PageTitleProperty); }
            set { SetValue(PageTitleProperty, value); }
        }

        public static readonly DependencyProperty AllowNavigationProperty = DependencyProperty.Register(
            "AllowNavigation", 
            typeof (bool), 
            typeof (MarkdownBrowser), 
            new PropertyMetadata(default(bool), (o, args) => ((MarkdownBrowser)o).AllowNavigation = (bool) args.NewValue));

        public bool AllowNavigation
        {
            get { return (bool) GetValue(AllowNavigationProperty); }
            set { SetValue(AllowNavigationProperty, value); }
        }

        public static readonly DependencyProperty ThemeProperty = DependencyProperty.Register(
            "Theme", typeof(MarkdownTheme), typeof(MarkdownBrowser), new FrameworkPropertyMetadata(default(MarkdownTheme))
            {
                PropertyChangedCallback = (o, args) => StaticTheme = (MarkdownTheme) args.NewValue
            });

        public MarkdownTheme Theme
        {
            get { return (MarkdownTheme)GetValue(ThemeProperty); }
            set { SetValue(ThemeProperty, value); }
        }

        public static readonly DependencyProperty MarkdownContentProperty = DependencyProperty.Register(
            "MarkdownContent",
            typeof(string),
            typeof(MarkdownBrowser),
            new FrameworkPropertyMetadata(default(string))
            {
                BindsTwoWayByDefault = true,
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                PropertyChangedCallback = MarkdownContentPropertyChanged
            });

        public string MarkdownContent
        {
            get { return (string)GetValue(MarkdownContentProperty); }
            set { SetValue(MarkdownContentProperty, value); }
        }

        private static void MarkdownContentPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            var browser = (MarkdownBrowser)o;
            var content = args.NewValue as string;
            if (!string.IsNullOrEmpty(browser.RootPath))
            {
                System.Diagnostics.Debug.WriteLine("Rewriting document links..");
                content = MarkdownUtility.RebuildLinks(content, browser.RootPath);
            }
            var page = new MarkdownBrowserPage(StaticPageTitle, content, StaticTheme);
            var transform = page.TransformText();
            browser.BrowserControl.Target = $"content://{transform}";
        }

        public static readonly DependencyProperty RootPathProperty = DependencyProperty.Register(
            "RootPath", typeof (string), typeof (MarkdownBrowser), new FrameworkPropertyMetadata(default(string))
            {
                BindsTwoWayByDefault = true,
                PropertyChangedCallback = (o, args) => ((MarkdownBrowser)o).RootPath = (string) args.NewValue
            });

        public string RootPath
        {
            get { return (string) GetValue(RootPathProperty); }
            set { SetValue(RootPathProperty, value); }
        }
    }

    public static class MarkdownUtility
    {
        private const string LinkMatchPattern = @"(\[[\w\s]+\])\((\w+\.md)\)";

        public static string RebuildLinks(string content, string linkPrefix)
        {
            var replaced = Regex.Replace(content, LinkMatchPattern, m => $"{m.Groups[1].Value}({linkPrefix}{m.Groups[2].Value})");
            //var name = Regex.Match(content, MatchPattern, RegexOptions.IgnoreCase).Groups[1].Value.Replace('[',' ').Replace(']',' ');
            return replaced;
        }
    }

    public enum MarkdownTheme
    {
        Readable,
        Amelia,
        Cerulean,
        Cyborg,
        Journal,
        Simplex,
        Slate,
        Spacelab,
        Spruce,
        Superhero,
        United,
        None
    }
}